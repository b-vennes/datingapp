import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import { User } from '../_models/user';
import { PaginatedResult } from '../_models/pagination';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserServiceMock {

  baseUrl = environment.apiUrl;

  getUsers(page?, itemsPerPage?): Observable<PaginatedResult<User[]>> {
    const mockUsers = new PaginatedResult<User[]>();
    mockUsers.result = [{
      age: 19,
      city: 'Tigard',
      country: 'US',
      created: undefined,
      gender: 'male',
      id: 0,
      interests: 'none',
      introduction: 'hello',
      knownAs: 'Bob',
      lastActive: undefined,
      lookingFor: 'unknown',
      photoUrl: 'test',
      photos: [],
      username: 'bob'
    }];

    return of(mockUsers);
  }

  getUser(id: number): Observable<User> {
    return of({
      age: 19,
      city: 'Tigard',
      country: 'US',
      created: undefined,
      gender: 'male',
      id: 0,
      interests: 'none',
      introduction: 'hello',
      knownAs: 'Bob',
      lastActive: undefined,
      lookingFor: 'unknown',
      photoUrl: 'test',
      photos: [],
      username: 'bob'
    });
  }

  updateUser(id: number, user: User) {

  }

  setMainPhoto(userId: number, id: number) {

  }

  deletePhoto(userId: number, id: number) {

  }
}
